function authenticate(call, callback) {
    const headers = call.metadata.getMap();
    if (!headers.hasOwnProperty('authorization')) {
        console.log(`UNAUTHEN`);
        return callback(new Error('Missing authorization header'), null);
    }
    // validate the token in the authorization header
    // ...
    callback(null, true);
}

module.exports = {
    authenticate
}