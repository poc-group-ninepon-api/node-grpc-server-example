require('dotenv').config()

const {
    createTodo,
    readTodos,
    createTodoClientStream,
    createTodoServerStream,
    readTodosStream,
    uploadFileStream
} = require("./src/handle")

const grpc = require("@grpc/grpc-js");
const protoLoader = require("@grpc/proto-loader");

//NOTE protoLoader
const packageDef = protoLoader.loadSync("todo.proto", {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true,
  });
const grpcObject = grpc.loadPackageDefinition(packageDef);
const todoPackage = grpcObject.todoPackage;

//NOTE start server
const server = new grpc.Server();
server.bindAsync(`${process.env.SERVER_GRPC_HOST}:${process.env.SERVER_GRPC_PORT}`, grpc.ServerCredentials.createInsecure(),
    (error, port) => {
        console.log(`Server running at http://${process.env.SERVER_GRPC_HOST}:${process.env.SERVER_GRPC_PORT}`);
        server.start();
    }
);
server.addService(todoPackage.TodoService.service, {
    "createTodo": createTodo,
    "createTodoServerStream": createTodoServerStream,
    "createTodoClientStream": createTodoClientStream,
    // "createTodoBiDirectonalStream": createTodoBiDirectonalStream,
    "readTodos": readTodos,
    "readTodosStream": readTodosStream,
    "uploadFileStream": uploadFileStream,
});